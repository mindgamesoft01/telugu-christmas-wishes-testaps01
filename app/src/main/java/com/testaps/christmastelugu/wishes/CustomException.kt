package com.testaps.christmastelugu.wishes

class CustomException(message: String) : Exception(message)
